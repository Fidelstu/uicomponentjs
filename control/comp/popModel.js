import $ from "../../asset/js/module/jquery.module.js";
import {PopModel} from "../../asset/js/module/PopModel.js";
import {Button} from "../../asset/js/module/Button.js";
import {Form} from "../../asset/js/module/Form.js";
import {LoadCss} from "../../asset/js/module/Load.js";


$(function () {   

new LoadCss("../../asset/css/form.css");
new LoadCss("../../asset/css/popModel.css");
new LoadCss("../../asset/css/button.css");

// let popForm = new Form("pop-form")
//   .setInner(
//     [
//       {
//         prompt: "prompt 1"
//         ,fieldNo: "field_1"
//       }
//       ,{
//         prompt: "prompt 2 "
//         ,fieldNo: "field_2"
//       }
//     ]
//   )
//   .dispatch();
let popId = "pop";

new PopModel(popId)
.setTitle("pop form title")
.setCss(
  {
    width: "30%"
  }
)
.setInner(
  new Form("pop-form")
  .setInner(
    [
      {
        prompt: "prompt 1"
        ,fieldNo: "field_1"
      }
      ,{
        prompt: "prompt 2 "
        ,fieldNo: "field_2"
      }
    ]
  )
  .dispatch()
)
.setFooter(
  [
    {
      text: "Ok"
      ,clickEvt: function(pop,evt) {  
        
        evt.preventDefault();   
        let itemInput = pop.inner.getItemInput();
  
        alert(JSON.stringify(itemInput));
        pop.inner.reset();
        pop.hide();
      }
    }
    ,
    {
      text: "Cancel"
      , closeEvt: true
      ,clickEvt: function(pop,evt) {  
        evt.preventDefault();   
        pop.hide();
      }
    }
    , 
    {
      text: "Reset"
      ,clickEvt: function(pop,evt) {  
        evt.preventDefault();  
        
        pop.inner.reset();
      }
    }
  ]
).create();

// pop.id = "pop"
// pop.title = "pop form title";
// //pop.height = "";
// //pop.width = "";
// pop.inner= popForm;
// pop.footer = [
//   {
//     text: "Ok"
//     ,clickEvt: function(pop,evt) {  
      
//       evt.preventDefault();   
//       let itemInput = pop.inner.getItemInput();

//       alert(JSON.stringify(itemInput));
//       pop.reset();
//       pop.hide();
//     }
//   }
//   ,
//   {
//     text: "Cancel"
//     ,clickEvt: function(pop,evt) {  
//       evt.preventDefault();   
//       pop.hide();
//     }
//   }
//   , 
//   {
//     text: "Reset"
//     ,clickEvt: function(pop,evt) {  
//       evt.preventDefault();  
      
//       pop.inner.reset();
//     }
//   }
// ]
// pop.create();


let btn = new Button("btn","Create Pop").setClickEvt(function(){
  $("#pop").show();
});
btn.create("body");



});