import $ from "../../asset/js/module/jquery.module.js";
import {Table} from "../../asset/js/module/Table.js";
import {LoadCss} from "../../asset/js/module/Load.js";

new LoadCss("../../asset/css/table.css");

$(function () {   
let table = new Table().setEditEvt(true).setCellsWidth(["10vh"]);

table.items=[
  {field_1:"value field 1"}
  ,{field_1:"value field 1"}
  ,{field_1:"value field 1"}
  ,{field_1:"value field 1",field_2:"value field 2"}
  ,{field_1:"value field 1"}
]

table.fields=[
      {
        prompt: "prompt 1"
        ,fieldNo: "field_1"
      }
      ,{
        prompt: "prompt 2 "
        ,fieldNo: "field_2"
      }

    ]

table.create(".con");

let items = table.getItemsDoc();

});