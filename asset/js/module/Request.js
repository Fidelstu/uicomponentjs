//request class
import $ from "./jquery.module.js";

class Request{
    constructor(webservice){
        this.webservice = webservice;
        this.args;
        this.request;

        return this;
    }

    setArgs(args){
        this.args = args;
        return this;
    }

    post(){        
        this.request = $.post( 
            this.webservice
            , this.args
        );

        return this;
    }

    get(){        
        this.request = $.get( 
            this.webservice
            , this.args
        );

        return this;
    }

    done(fnSuccess){
        this.request.done(fnSuccess);
        return this;
    }

    fail(fnFail){
        this.request.fail(fnFail);
        return this;
    }

    final(fnFinal){
        this.request.always(fnFinal);
    }

}

export {Request};