//table class
import $ from "./jquery.module.js";
import {Button} from "./Button.js";

class Table{
    constructor(){
        this._items = [];
        this._fields = [];
        //this.con = $(container);        
        this.columnCount = 1;
        this.className = "data-table";
        this.bindTarget = [];

        this.cellsWidth = [];//["10vh"];
    }
    setCellsWidth(widths){
        this.cellsWidth = widths;
        return this;
    }

    setEditEvt(editEvt){
        this.editEvt = editEvt;
        return this;
    }
    
    setTarget(...targets){
        this.bindTarget = [].concat(...targets);
        return this;
    }

    get items() {
     return this._items;
    }

    set items(items) {
        this._items = items;
    }

    setItems(items){
        this.items = items;
        return this;
    }

    get fields() {
        return this._fields;
        }
   
    set fields(fields) {
        this._fields = fields;
    }   

    setFields(fields){
        this.fields = fields;
        return this;
    }

    addItem(item){
        let table = this.html;
        let tBody = table.find("tbody");
        let rowsEl = table.find("tr");

        let newRow = $("<tr>",{_new: true});

        const fields = this.fields;

        $.each(fields,function(iField, objField){
            let newCell = objField ? $("<td>",{fieldNo:objField.fieldNo, text:objField.fieldNo ? item[objField.fieldNo] : "" }) : $("<td>");
            newRow.append(newCell);
        });
        tBody.append(newRow);
    }

    editItem(iRow,item){
        let table = this.html;
        let rowsEl = table.find("tr");
        const fields = this.fields;

        let rowEl = tabel.find("tbody tr."+iRow);
        rowEl.attr("_edit",true)
            
        $.each(fields, function(i, cell) {
            let cellEl = rowEl.find("td[fieldNo="+cell.fieldNo+"]");
            
            if(cellEl.length != 0 && item[cell.fieldNo] !== cellEl.text()){
                cellEl.text(item[cell.fieldNo]);
            }
        });
    }

    deleteItem(iRow,item){
        let table = this.html;
        let rowsEl = table.find("tr");
        const fields = this.fields;

        let rowEl = tabel.find("tbody tr."+iRow);
            
        rowEl.attr("_delete",true).hide();
    }

    getItemsDoc(){
        const fields = this.fields;
        let table = this.html;
        let dataTable = [];
               
        let rowsEl = table.find("tr");

        rowsEl.each(function(iRow,objRow){
            let cellsEl = $(this).find("td");
            
            if(cellsEl.length > 0){
                let row = {};
                cellsEl.each(function(iCell,objCell){
                    const fieldNo = $(this).attr("fieldNo");
                    row[fieldNo] = objCell.textContent;

                })
                dataTable.push(row);
            }
        });
                  
        return dataTable;
    }

    commit(){        
        const items = this._items;
        const fields = this._fields;
        let tabel = this.html;
        debugger
        //commit table header
        $.each(fields, function(i, cell) {
            let cellHeaderEl = tabel.find("thead th[fieldNo="+cell.fieldNo+"]");
            
            if(cellHeaderEl.length != 0 && cell.prompt !== cellHeaderEl.text()){
                cellHeaderEl.text(cell.prompt);
            }
        });

        //commit table data
        $.each(items, function(i, rowData) {
            let rowEl = tabel.find("tbody tr."+i);
            
            $.each(fields, function(i, cell) {
                let cellEl = rowEl.find("td[fieldNo="+cell.fieldNo+"]");
                
                if(cellEl.length != 0 && rowData[cell.fieldNo] !== cellEl.text()){
                    cellEl.text(rowData[cell.fieldNo]);
                }
            });
        });

             
    }
    
    dispatch(){
        const items = this._items;
        const inner = this._fields;

        const editEvt = this.editEvt;
        
        let tabel = $("<table>",{class:this.className, id:this.id});
        //cell width
        let colGroup = $("<colgroup>");
        $.each(this.cellsWidth, (i,width)=>{
            $("<col>").css({width:width}).appendTo(colGroup);
            debugger
        })
        colGroup.appendTo(tabel);
        
        //create table header
        let tHead = $("<thead>");

        let row = $("<tr>");
        $.each(inner, function(index, obj) {                
            let cellHeader = "";
            if(index == 0 & editEvt){
                cellHeader = $("<th>");
                row.append(cellHeader);
            }

            cellHeader = obj ? $("<th>",{text:obj.prompt, fieldNo:obj.fieldNo}) : $("<th>");


            row.append(cellHeader);
        });
        tHead.append(row);
        tabel.append(tHead);
        
        //create table body
        let tBody = $("<tbody>");
        $.each(items, function(indexItem, objItem) {
            row = $("<tr>",{class:indexItem+""});

            let cellData = "";
            if(editEvt){
                let editCell = $("<th>");

                let editBtn = new Button("btn","edit"); //.setTarget(...bindTarget).setClickEvt(btnEvtCall);

                editCell.append(editBtn.html);
                row.append(editCell);
            }

            $.each(inner, function(index, obj) {                

                cellData = obj ? $("<td>",{fieldNo:obj.fieldNo, text:obj.fieldNo ? objItem[obj.fieldNo] : "" }) : $("<td>");
                row.append(cellData);
            });

            tBody.append(row);
        });
        tabel.append(tBody);               
        
        this.html = tabel;
    }

    create(con){
        this.dispatch();
        this.html.appendTo(con);
        return this;
    }

    createBefore(elemSelect){
        this.dispatch();
        this.html.insertBefore(elemSelect)
        return this;
    }


    

}

export {Table};