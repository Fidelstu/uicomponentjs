//form class
import $ from "./jquery.module.js";
import {Button} from "./Button.js";

class PopModel{
    constructor(id){
        this.id = id;
        this.title = "";
        this.inner = "component";
        this.footer = [];
        this.bindTarget=[];
        //this.cssCfg = {};
       // this.con = $(container);  
    }

    setTarget(...args){
        debugger
        let targets = [...args];
        this.bindTarget= targets;
        return this;
    }

    setTitle(title){
        this.title = title;
        return this;
    }
    
    setInner(inner){
        this.inner = inner;
        return this;
    }

    setCss(css){
        this.cssCfg = css;
        return this;
    }

    setFooter(footer){
        this.footer = footer;
        return this;
    }

    show(){
        $("#"+this.id).show();
        return this;
    }
    hide(){
        $("#"+this.id).hide();
        return this;
    }
    
    dispatch(){
        let PopModel = this;
        let popCon = $("<div>",{id:this.id, class:"pop-con"});
       
        let popContent = $("<div>",{class:"pop-content"});    
        
        this.cssCfg && popContent.css(this.cssCfg);

        //pop header
        const title = this.title;
        let popHeader = $("<div>",{class:"pop-header"});
        let tabel = $("<table>");
        let tBody = $("<tbody>");
        let tRow = $("<tr>");

        debugger
        let titleEl = $("<td>",{class:"pop-title",text:title});
        titleEl.appendTo(tRow);


        let closeX = $("<td>",{class:"pop-close",text:"x"});
        let closeEvt = function(pop,evt){            
            pop.hide();
        }.bind(closeX,PopModel)

        

        
        closeX.appendTo(tRow);

        tBody.append(tRow).appendTo(tabel);
        popHeader.append(tabel).appendTo(popContent);
        //-----------------------------------------------------

        //pop body
        let popBody = $("<div>",{class:"pop-body"});        
        popBody.append(this.inner.html).appendTo(popContent);
        //-----------------------------------------------------
        
        //pop footer
        let popFooter = $("<div>",{class:"pop-footer"});
        const footer = this.footer;

        debugger
        let bindTarget = [PopModel].concat(this.bindTarget);
         $.each(footer, function(index, obj) {  
            //const footBtn = obj.html;

            const className = obj.className ? obj.className : "btn";
            const textBtn = obj.text ? obj.text : "";
           
            let btnEvtCall = obj.clickEvt;

            if(obj.closeEvt){                
                debugger
                let target = [closeX].concat(bindTarget);
                closeEvt = btnEvtCall.bind(...target);                
            }            

            
            let btn = new Button(className,textBtn).setTarget(...bindTarget).setClickEvt(btnEvtCall);
            popFooter.append(btn.html);


         });

         closeX.click(closeEvt);

         popFooter.appendTo(popContent);
         //-----------------------------------------------------

         popCon.append(popContent);               

        
        this.html = popCon;
    }

    create(){
        this.dispatch();
        this.html.appendTo("body");
        return this;
    }



    

}

export {PopModel};