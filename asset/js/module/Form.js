//form class
import $ from "./jquery.module.js";

class Form{
    constructor(id){
        this.id = id;
        this._item = {};
        this._inner = [];
       // this.con = $(container);        
        this.columnCount = 1;
        return this;
    }

    setColumn(qtPerRow){
        this.columnCount = qtPerRow;
        return this;
    }
   

    get item() {
     return this._item;
    }

    set item(item) {
        this._item = item;
    }

    get inner() {
        return this._inner;
       }
   
    set inner(inner) {
        this._inner = inner;
    }

    setInner(inner){
        this.inner = inner;

        return this;
    }

    setItem(param1,val){
        if(typeof param1 === 'string' || param1 instanceof String){
            let objItem = this._item;
            objItem[param1] = val;
           
        }else{
            this._item = param1;
        }

        return this;
    }

    onSubmit(evt){
        evt.preventDefault();
        debugger

        return this;
    }

    commit(){        
        const item = this._item;
        let form = $("#"+this.id);
        

        $.each(this.inner, function(index, obj) {  
            debugger
            let label = form.find("th[fieldNo="+obj.fieldNo+"]");
            if(label.length != 0 && label.text() !== obj.prompt){
                label.text(obj.prompt);
            }

            let ipt = form.find("input[fieldNo="+obj.fieldNo+"]");
            if(ipt.length != 0 && ipt.val() !== item[obj.fieldNo]){
                ipt.val(item[obj.fieldNo]);
            }

        });


        for(const prop in item){
            let label = form.find("th[fieldNo="+prop+"]");
            let ipt = form.find("input[fieldNo="+prop+"]");
            
            if(ipt.length != 0 && ipt.val() !== item[prop]){
                ipt.val(item[prop]);
            }
        }
        
        
    }

    reset(){
        this.commit();
    }

    getItemInput(){
        let form = $("#"+this.id);
        let inputForm = {};

        $.each(this.inner, function(index, obj) {  
            let ipt = form.find("input[fieldNo="+obj.fieldNo+"]");

            inputForm[obj.fieldNo] = ipt.val();
        });

        return inputForm;
    }
    
    dispatch(){
        let formEl = $("<form>",{id:this.id});
        const item = this._item;

        let tabel = $("<table>",{class:"form-table"});
        let columnCount= this.columnCount;
        let count= columnCount;

        let tBody = $("<tbody>");
        let row = $("<tr>");
        debugger
        $.each(this.inner, function(index, obj) {  
            
            if(obj.type != "button"){
                let labelEl = $("<th>",{class:"form-label", html:obj.prompt, fieldNo:obj.fieldNo});
                //labelEl.attr({class:"form-label", html:obj.prompt, fieldNo:obj.fieldNo});
                row.append(labelEl);

            }

            let lnputEl = "";

            if(obj.type == "button"){
                lnputEl = $("<input>",{class:"form-input btn", type:obj.type || "text", value: obj.text});
            }else{

                lnputEl = $("<input>",{class:"form-input", type:obj.type || "text", value: item[obj.fieldNo], fieldNo:obj.fieldNo});
            
            };

            let inputElTd =$("<td>").append(lnputEl);
            


            row.append(inputElTd);
            count-= 1;
            if(count == 0){      
                tBody.append(row);

                row = $("<tr>");
                count= columnCount;
            }

            
        });

        /*
        let btnSubmit = $("<input>",{class:"form-submit", type:"submit", value:"Submit"});        
        row.append($("<td>").append(btnSubmit));       
        
        tabel.append(tBody.append(row));
        */

        tabel.append(tBody);

        formEl.append(tabel);
        //formEl.append(btnSubmit);

        formEl.submit(this.onSubmit.bind(this));

        
        this.html = formEl;

        return this;
    }

    create(con){
        this.dispatch();
        this.html.appendTo(con);
        return this;
    }

    createBefore(elemSelect){
        this.dispatch();
        this.html.insertBefore(elemSelect);
        return this;
    }


    

}

export {Form};