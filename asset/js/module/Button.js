//form class
import $ from "./jquery.module.js";

class Button{
    constructor(className,text){
        this.html = $("<button>",{class:className,text:text});
       // this.id = "";
        this.bindTarget = [];
        this.clickEvt;

        return this;
    }
    

    get onClick() {
        return this._onClick;
    }
   
    set onClick(clickFn) {  

        this._onClick = clickFn;
        this.click();
    }

    setCss(css){
        //this.cssCfg = css;
        
        this.html.css(css); 
        return this;
    }

    setTarget(...args){
        let targets = [...args];
        this.bindTarget= targets;
        return this;
    }

    setClickEvt(ability){
        let targets = [this.html].concat(this.bindTarget);
        this.html.click(ability.bind(...targets));  
        return this;
    }

    click(){
        debugger
        let targets = [this.html].concat(this.bindTarget);
        this.html.click(this.onClick.bind(...targets));         
    }  


    commit(con){        
         this.html.appendTo(con);     
         return this; 
    }

    setId(id){
        this.id=id;
        return this;
    }
    
    dispatch(){
        this.id && this.html.attr("id",this.id);               
    }

    create(con){
        this.dispatch();
        this.html.appendTo(con);
    }
   


    

}

export {Button};