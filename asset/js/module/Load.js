//form class
import $ from "./jquery.module.js";

class LoadCss{
    constructor(pathCss){
        $("<link/>", {
            rel: "stylesheet",
            type: "text/css",
            href: pathCss
         }).appendTo("head");

    }  

}

export {LoadCss};